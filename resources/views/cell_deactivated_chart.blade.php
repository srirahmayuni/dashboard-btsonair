<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="{{url('')}}/css/app.css">
        <link rel="stylesheet" href="{{url('')}}/css/style.css">
        <link rel="stylesheet" href="{{url('')}}/custom/css/adminlte.min.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    </head>
    <body>
        <div class="row">
            <div class="card-body">
                <div class="chart" style="height:170px">
                    <canvas id="lineChart2"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/custom/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/custom/js/demo.js"></script>
    <!-- jQuery -->
    <script src="{{url('')}}/custom/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- <script src="custom/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- ChartJS 1.0.1 -->
    <script src="{{url('')}}/custom/chartjs-old/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/custom/fastclick/fastclick.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'{{url("cell_deactivated")}}',
            type:'get',
            success: function(result){
                console.log(result.license);
        var areaChartData = {
        labels  :[
            <?php
            foreach ($label_cell as $label) {
                echo '"'.$label.'",';
            }
            ?>
        ],
        datasets: [
            {
                label               : 'Digital Goods',
                fillColor           : 'rgba(0,0,0,0.5)',
                strokeColor         : 'rgba(0,0,0,0.5)',
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(0,0,0,0.5)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(0,0,0,0.5)',
                data                : [
                    <?php
                    foreach ($value_cell as $value) {
                        echo $value.',';
                    }
                    ?>
                ]
            }
        ]
    }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)
  }
    </script>
</html>
